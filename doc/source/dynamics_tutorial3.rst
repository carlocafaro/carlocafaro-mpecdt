Computational Lab: Dynamics 3
=============================

This is the tutorial material for Computational Lab 3 in the Dynamics
core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpcdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

This Computational Lab is about invariant measures and ergodicity.

We are going to continue to use the Lorenz system as an example, given by

.. math::

   \begin{eqnarray}
   \dot{x} &= \sigma (y-x)\\
   \dot{y} &= x(\rho-z) - y  \\ 
   \dot{z} &= xy - \beta z
   \end{eqnarray}

and parameter values :math:`\sigma = 10,\,\rho = 28`, and :math:`\beta = 8/3`.

However, we are now going to take a more probabilistic view of the
dynamics. For a dynamical system 

.. math::
   \dot{x} = f(x),

say that our initial condition is not known precisely, but is assumed
to be a random variable with PDF :math:`\pi_0(x)`. At later times, the
variable evolves according to the dynamical system, and has PDF
:math:`\pi_t(x)` at time :math:`t`, which satisfies the Liouville
equation,

.. math::
   \frac{\partial}{\partial t}\pi_t + \sum_{i=1}^d\frac{\partial}{\partial x_i}(f_i\pi_t) = 0,

where :math:`(x_1,x_2,\ldots,x_d)` are the components of :math:`x`,
which is :math:`d` -dimensional, and :math:`(f_1,f_2,\ldots,f_d)` are
the components of :math:`f(x)`. This equation allows us to do
probabilistic forecasting by propagating our uncertainty in the form
of a PDF forwards in time to provide probabilities of the future state
of the system.

The cost of solving this PDE using a grid-based method (such as finite
difference or finite element) grows exponentially with the dimension
:math:`d`. An alternative is to use a Monte Carlo approximation. We 
replace our initial PDF :math:`\pi_0(x)` by the empirical distribution

.. math::
   \pi(x) = \frac{1}{M}\sum_{i=1}^M \delta(x-X_i(0)),

where the variables :math:`\{X_i(0)\}_{i=1}^M` are independent,
individually distributed random variables drawn from
:math:`\pi_0`. These variables are referred to as "particles" or
"ensemble members".

At later times, the approximation satisfies

.. math::
   \pi(x) = \frac{1}{M}\sum_{i=1}^M \delta(x-X_i(t)),

where each of the variables :math:`\{X_i(0)\}_{i=1}^M` are solutions
of the dynamical system. The only source of error in this
approximation is the initialisation, and timestepping error in solving
the dynamical system.

Exercise 1
----------

.. container:: tasks  

   Add a new file to the mpecdt/src directory called `ergodic.py`, in
   which you should add all of the tasks from this tutorial. You can add
   this file to the repository using ::
     git add ergodic.py
     git commit

.. container:: tasks  

   Use the Monte Carlo method to solve the probabilistic forecasting
   problem for the Lorenz equation, using an initial condition which
   is the normal distribution with mean zero and covariance matrix
   equal to the identity multiplied by 0.1, with :math:`M=100`. Make
   use of the solution function and 3D plotting function that you used
   in the first lab. Visualise this solution at :math:`t=0`,
   :math:`t=10`, :math:`t=100` by plotting the location of the
   ensemble members. If feasible increase :math:`M` to obtain a more
   accurate solution.

It is known that the solution of the Liouville equation for the Lorenz
system converges to an invariant measure :math:`\mu(x)` as
:math:`t\to\infty` under very general assumptions on the initial
conditions for :math:`\pi_0(x)`. The invariant measure is supported on
the Lorenz attractor. Hence, for large times, our Monte Carlo method
produces an empirical approximation to the Lorenz invariant measure.

Exercise 2
----------

The Lorenz system is known to be ergodic. This means that, for any
(sensible) function :math:`f:\mathbb{R}^3\to \mathbb{R}`, we have

.. math::
   \lim_{T\to \infty}\frac{1}{T}\int_0^T f(X(t)) dt =
   \int_{\mathbb{R}^3} f(x) d\mu(x),

where :math:`X(t)` is a solution to the Lorenz equations, and
:math:`\mu` is the invariant measure of the Lorenz equations. This
means that time averages of functions along solutions of the Lorenz
equations are equal to space averages weighted by the invariant
measure.

We can approximate the time average numerically by taking a numerical
solution :math:`\{X^n\}_{n=1}^N` at discrete times :math:`t^n, \quad n=1,\ldots N`, and computing

.. math::
  \frac{1}{N} \sum_{n=1}^N f(X^n).

.. container:: tasks  
	       
   Write a function to compute this time average.

We can approximate the spatial average using our Monte Carlo
approximation in the previous exercise.

.. math::
  \frac{1}{M} \sum_{i=1}^M f(X_i(T)),

for some large time :math:`T`.

.. container:: tasks
	       
   Write a function to compute this time average. Choose a function
   :math:`f` which is not zero on the attractor and verify that these two
   sums produce similar answers for sufficiently large :math:`N,T,M`.

Exercise 3
----------

If we make an analogy between predicting the Lorenz equations and
weather forecasting, then the change in the solution represents the
weather, whilst the invariant measure represents the climate. We are
now going to create some climate change! This is done by changing the
parameters :math:`\rho,\sigma,\beta` for the Lorenz equation.

.. container:: tasks

   Visualise the invariant measure (either by computing a long time
   solution and sampling values at various points in time, or by
   propagating an ensemble of state values) for various different
   :math:`\rho,\sigma,\beta`. What happens to the shape of the
   attractor? Which parameter is the attractor shape most sensitive
   to?

.. container:: tasks
   
We would like to compute the linear response of the invariant measure,
i.e., how does the invariant measure change with small changes in the
parameters? This is difficult to observe through ensemble plots, so
we normally measure this using different functions :math:`f`.

.. container:: tasks
	       
   Choose a test function :math:`f` and plot the change in 

   .. math::
   
      \mathbb{E}_{\mu}[f(X)] = \int f(x) d\mu(x)

   under changes in :math:`\rho` around our standard value of
   :math:`\rho=28`. Identify if there is a region where the response
   is linear in the variable :math:`\rho'`, where
   :math:`\rho=28+\rho`. Compute the partial derivative of the
   expectation of :math:`f` with respect to :math:`\rho`. Perform
   similar calculations for the other two parameters. Repeat this
   calculation for other test functions :math:`f`. Which is the most 
   sensitive direction in :math:`(\rho,\sigma,\beta)` space? Is the 
   response always linear? Are there any points where the response
   is non-differentiable?
